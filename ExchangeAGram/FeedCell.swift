//
//  FeedCell.swift
//  ExchangeAGram
//
//  Created by Elton Nix on 6/27/15.
//  Copyright (c) 2015 Rare Agenda. All rights reserved.
//

import UIKit

class FeedCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    
    
}
